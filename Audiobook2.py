import os
import pyttsx3

engine = pyttsx3.init()

voices = engine.getProperty('voices')
for voice in voices:
    print("Voice:")
    print(" - ID: %s" % voice.id)
    print(" - Name: %s" % voice.name)
    print(" - Languages: %s" % voice.languages)
    print(" - Gender: %s" % voice.gender)
    print(" - Age: %s" % voice.age)


os.chdir("C:/Users/juanm/Documents/Python/Python Audiobooks")

book = open('The New Life (La Vita Nuova) by Dante Alighieri.txt', 'r', encoding=("utf-8"))
text = book.read().replace("\n", " ")

en_voice_id = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0"

speaker = pyttsx3.init()
speaker.setProperty('voice', en_voice_id)
speaker.setProperty("rate", 200)
speaker.say(text)
speaker.runAndWait()

# #Prueba en español
# es_voice_id = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_ES-MX_SABINA_11.0"

# speaker = pyttsx3.init()
# speaker.setProperty('voice', es_voice_id)
# speaker.setProperty("rate", 190)
# speaker.say("Maria Camila yo te amo mucho mucho mucho, espero que estés pasando muy bien en Miami, te veo el domingo")
# speaker.runAndWait()
