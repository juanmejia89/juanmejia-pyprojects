import os
import pyttsx3
import PyPDF2

# Check for available voices in the system:
import pyttsx3
engine = pyttsx3.init()

voices = engine.getProperty('voices')
for voice in voices:
    print("Voice:")
    print(" - ID: %s" % voice.id)
    print(" - Name: %s" % voice.name)
    print(" - Languages: %s" % voice.languages)
    print(" - Gender: %s" % voice.gender)
    print(" - Age: %s" % voice.age)


os.chdir("C:\\Users\\juanm\\Documents\\Python\\Python Audiobooks")

book = open('the wall2.pdf', 'rb')
pdfReader = PyPDF2.PdfFileReader(book)
pages = pdfReader.numPages
print(pages)

speaker = pyttsx3.init()

en_voice_id = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0"

for num in range(0, pages):
    page = pdfReader.getPage(num)
    text = page.extractText().replace("\n", " ")
    speaker.setProperty('voice', en_voice_id)
    speaker.setProperty("rate", 190)
    speaker.say(text)
    speaker.runAndWait()

#Prueba en español
es_voice_id = "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_ES-MX_SABINA_11.0"

speaker = pyttsx3.init()
speaker.setProperty('voice', es_voice_id)
speaker.setProperty("rate", 190)
speaker.say("Maria Camila yo te amo mucho mucho mucho, espero que estés pasando muy bien en Miami, te veo el domingo")
speaker.runAndWait()
